export interface ITask {
  id: number;
  name: string;
  description: string;
  dueDate: Date;
  status: "Overdue" | "Due soon" | "Not urgent";
  completed: boolean;
  completedAt: string;
  createdAt: string;
  updatedAt: string;
}

export interface ITaskFilterVariables {
  name: string;
}
