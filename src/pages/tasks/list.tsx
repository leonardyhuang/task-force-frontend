import {
  Button,
  Col,
  DateField,
  DeleteButton,
  EditButton,
  Form,
  FormProps,
  getDefaultSortOrder,
  Icons,
  Input,
  List,
  Row,
  Space,
  Table,
  TagField,
  useTable,
} from "@pankod/refine-antd";
import { CrudFilters, HttpError, useApiUrl } from "@pankod/refine-core";
import { ITask, ITaskFilterVariables } from "interfaces";

export const TaskList: React.FC = () => {
  const { tableProps, sorter, searchFormProps } = useTable<
    ITask,
    HttpError,
    ITaskFilterVariables
  >({
    onSearch: (params) => {
      const filters: CrudFilters = [];
      const { name } = params;

      filters.push({
        field: "name",
        operator: "contains",
        value: name,
      });

      return filters;
    },
  });

  const apiUrl = useApiUrl();
  const markComplete = (record: ITask) => {
    fetch(`${apiUrl}/tasks/${record.id}/mark-complete`, {
      method: "PUT",
    });
  };
  const markIncomplete = (record: ITask) => {
    fetch(`${apiUrl}/tasks/${record.id}/mark-incomplete`, {
      method: "PUT",
    });
  };

  return (
    <Row>
      <Col>
        <Filter formProps={searchFormProps} />
      </Col>
      <List>
        <Table {...tableProps} rowKey="id">
          <Table.Column dataIndex="name" title="Name" />
          <Table.Column dataIndex="description" title="Description" />
          <Table.Column
            dataIndex="dueDate"
            title="Due Date"
            render={(value) => <DateField format="LL" value={value} />}
            sorter
            defaultSortOrder={getDefaultSortOrder("dueDate", sorter)}
          />
          <Table.Column
            dataIndex="status"
            title="Status"
            render={(value) => (
              <TagField
                value={value}
                color={
                  value === "Overdue"
                    ? "red"
                    : value === "Due soon"
                    ? "yellow"
                    : "green"
                }
              />
            )}
          />
          <Table.Column
            dataIndex="completed"
            title="Completed"
            render={(value) => (
              <TagField
                value={value ? "Completed" : "Not completed"}
                color={value ? "green" : "red"}
              />
            )}
          />
          <Table.Column
            dataIndex="completedAt"
            title="Completed At"
            render={(value) =>
              value ? <DateField format="LLL" value={value} /> : null
            }
          />
          <Table.Column
            dataIndex="createdAt"
            title="Created At"
            render={(value) => <DateField format="LLL" value={value} />}
            sorter={{ multiple: 1 }}
            defaultSortOrder={getDefaultSortOrder("createdAt", sorter)}
          />
          <Table.Column
            dataIndex="updatedAt"
            title="Updated At"
            render={(value) => <DateField format="LLL" value={value} />}
          />
          <Table.Column<ITask>
            dataIndex="actions"
            title="Actions"
            render={(_text, record): React.ReactNode => {
              return (
                <Space>
                  <Button
                    size="small"
                    type={record.completed ? "ghost" : "primary"}
                    onClick={() => {
                      record.completed
                        ? markIncomplete(record)
                        : markComplete(record);
                      window.location.reload();
                    }}
                  >
                    {record.completed ? "Mark incomplete" : "Mark complete"}
                  </Button>
                  <EditButton size="small" recordItemId={record.id} hideText />
                  <DeleteButton
                    size="small"
                    recordItemId={record.id}
                    hideText
                  />
                </Space>
              );
            }}
          />
        </Table>
      </List>
    </Row>
  );
};

const Filter: React.FC<{ formProps: FormProps }> = ({ formProps }) => {
  return (
    <Form layout="vertical" {...formProps}>
      <Form.Item label="Search" name="name">
        <Input
          placeholder="Name starts with..."
          prefix={<Icons.SearchOutlined />}
        />
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit" type="primary">
          Search
        </Button>
      </Form.Item>
    </Form>
  );
};
