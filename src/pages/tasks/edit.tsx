import { DatePicker, Edit, Form, Input, useForm } from "@pankod/refine-antd";
import dayjs from "dayjs";
import { ITask } from "interfaces";

export const TaskEdit: React.FC = () => {
  const { formProps, saveButtonProps } = useForm<ITask>();

  return (
    <Edit saveButtonProps={saveButtonProps}>
      <Form {...formProps} layout="vertical">
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              max: 191,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Description"
          name="description"
          rules={[
            {
              required: true,
              max: 191,
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Due Date"
          name="dueDate"
          getValueProps={(value) => ({
            value: value ? dayjs(value) : "",
          })}
          rules={[
            {
              required: true,
            },
          ]}
        >
          <DatePicker />
        </Form.Item>
      </Form>
    </Edit>
  );
};
