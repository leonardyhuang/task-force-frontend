import {
  ErrorComponent,
  Layout,
  notificationProvider,
  ReadyPage,
} from "@pankod/refine-antd";
import { Refine } from "@pankod/refine-core";

import "@pankod/refine-antd/dist/styles.min.css";
import routerProvider from "@pankod/refine-react-router-v6";
import dataProvider from "@pankod/refine-simple-rest";
import { TaskCreate } from "pages/tasks/create";
import { TaskEdit } from "pages/tasks/edit";
import { TaskList } from "pages/tasks/list";

function App() {
  return (
    <Refine
      notificationProvider={notificationProvider}
      Layout={Layout}
      ReadyPage={ReadyPage}
      catchAll={<ErrorComponent />}
      routerProvider={routerProvider}
      dataProvider={dataProvider(process.env.REACT_APP_API_BASE_URL as string)}
      resources={[
        {
          name: "tasks",
          list: TaskList,
          create: TaskCreate,
          edit: TaskEdit,
          canDelete: true,
        },
      ]}
    />
  );
}

export default App;
